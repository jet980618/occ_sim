const fs = require('fs');
const MQTT = require("async-mqtt");
const csvjson = require('csvjson');
const simBindVideo = require('./simBindVideo.js');
const config = JSON.parse(fs.readFileSync("config/config.json", { encoding: 'utf8' }));
const videoConfig = require('./video.json');
const exec = require('child_process').exec;

let ncount = 0;

let options = {
    delimiter: ','
};

let useErrorData = config.useErrorData;
let dataFolder = config.dataFolder;
if (useErrorData === true) {
    dataFolder = config.errDataFolder;
}
let simIntervaMillisec = config.simIntervaMillisec;

let cameraCsv = dataFolder + 'camera.csv';
let acceleratorPosCsv = dataFolder + 'accelerator_pos.csv';
let brakePosCsv = dataFolder + 'brake_pos.csv';
let steeringWheelAngleCsv = dataFolder + 'steering_wheel_angle.csv';
let turnSignalPosCsv = dataFolder + 'turn_signal_pos.csv';
let brakeLightPosCsv = dataFolder + 'brake_light_pos.csv';
let engineLoadCsv = dataFolder + 'engine_load.csv';
let coolantTempCsv = dataFolder + 'coolant_temp.csv';
let controlModuleVoltageCsv = dataFolder + 'control_module_voltage.csv';
let intakeTempCsv = dataFolder + 'intake_temp.csv';
let vinCsv = dataFolder + 'vin.csv';
let engineLightCsv = dataFolder + 'engine_light.csv';
let runTimeCsv = dataFolder + 'run_time.csv';
let rpmCsv = dataFolder + 'rpm.csv';
let dtcCsv = dataFolder + 'dtc.csv';
let fuelLevelCsv = dataFolder + 'fuel_level.csv';
let gearStateCsv = dataFolder + 'gear_state.csv';
let mainLightPosCsv = dataFolder + 'main_light_pos.csv';
let imuCsv = dataFolder + 'imu.csv';
let bmsCsv = dataFolder + 'bms.csv';
let radarStatusCsv = dataFolder + 'radar_status.csv';
let radarSrrCsv = dataFolder + 'radar_srr.csv';
let radarEsrCsv = dataFolder + 'radar_esr.csv';
let lidarCsv = dataFolder + 'lidar.csv';
let gnssCsv = dataFolder + 'gnss.csv';
let speedCsv = dataFolder + 'wheel_speed_log.csv';
let doCsv = dataFolder + 'detected_object_log.csv';
let doMultiDataJson = dataFolder + 'detected_multi_object_log.json';
let drivingModeCsv = dataFolder + 'driving_mode.csv';
let mileageCsv = dataFolder + 'mileage.csv';

let cameraData = readCsv(cameraCsv);
let acceleratorPosData = readCsv(acceleratorPosCsv);
let brakePosData = readCsv(brakePosCsv);
let steeringWheelAngleData = readCsv(steeringWheelAngleCsv);
let turnSignalPosData = readCsv(turnSignalPosCsv);
let brakeLightPosData = readCsv(brakeLightPosCsv);
let engineLoadData = readCsv(engineLoadCsv);
let coolantTempData = readCsv(coolantTempCsv);
let controlModuleVoltageData = readCsv(controlModuleVoltageCsv);
let intakeTempData = readCsv(intakeTempCsv);
let vinData = readCsv(vinCsv);
let engineLightData = readCsv(engineLightCsv);
let runTimeData = readCsv(runTimeCsv);
let rpmData = readCsv(rpmCsv);
let dtcData = readCsv(dtcCsv);
let fuelLevelData = readCsv(fuelLevelCsv);
let gearStateData = readCsv(gearStateCsv);
let mainLightPosData = readCsv(mainLightPosCsv);
let imuData = readCsv(imuCsv);
let bmsData = readCsv(bmsCsv);
let radarStatusData = readCsv(radarStatusCsv);
let radarSrrData = readCsv(radarSrrCsv);
let radarEsrData = readCsv(radarEsrCsv);
let lidarData = readCsv(lidarCsv);
let gnssData = readCsv(gnssCsv);
let speedData = readCsv(speedCsv);
let doData = readCsv(doCsv);
let doMultiData = {};
let drivingModeData = readCsv(drivingModeCsv);
let mileageData = readCsv(mileageCsv);

let cameraJson = csvjson.toObject(cameraData, options);
let acceleratorPosJson = csvjson.toObject(acceleratorPosData, options);
let brakePosJson = csvjson.toObject(brakePosData, options);
let steeringWheelAngleJson = csvjson.toObject(steeringWheelAngleData, options);
let turnSignalPosJson = csvjson.toObject(turnSignalPosData, options);
let brakeLightPosJson = csvjson.toObject(brakeLightPosData, options);
let engineLoadJson = csvjson.toObject(engineLoadData, options);
let coolantTempJson = csvjson.toObject(coolantTempData, options);
let controlModuleVoltageJson = csvjson.toObject(controlModuleVoltageData, options);
let intakeTempJson = csvjson.toObject(intakeTempData, options);
let vinJson = csvjson.toObject(vinData, options);
let engineLightJson = csvjson.toObject(engineLightData, options);
let runTimeJson = csvjson.toObject(runTimeData, options);
let rpmJson = csvjson.toObject(rpmData, options);
let dtcJson = csvjson.toObject(dtcData, options);
let fuelLevelJson = csvjson.toObject(fuelLevelData, options);
let gearStateJson = csvjson.toObject(gearStateData, options);
let mainLightPosJson = csvjson.toObject(mainLightPosData, options);
let imuJson = csvjson.toObject(imuData, options);
let bmsJson = csvjson.toObject(bmsData, options);
let radarStatusJson = csvjson.toObject(radarStatusData, options);
let radarSrrJson = csvjson.toObject(radarSrrData, options);
let radarEsrJson = csvjson.toObject(radarEsrData, options);
let lidarJson = csvjson.toObject(lidarData, options);
let gnssJson = csvjson.toObject(gnssData, options);
let speedJson = csvjson.toObject(speedData, options);
let doJson = csvjson.toObject(doData, options);
let doMultiJson = [];
let drivingModeJson = csvjson.toObject(drivingModeData, options);
let mileageJson = csvjson.toObject(mileageData, options);

let isCameraEnabled = config.sensor.isCameraEnabled;
let isBmsEnabled = config.sensor.isBmsEnabled;
let isEcuEnabled = config.sensor.isEcuEnabled;
let isGnssEnabled = config.sensor.isGnssEnabled;
let isImuEnabled = config.sensor.isImuEnabled;
let isRadarEnabled = config.sensor.isRadarEnabled;
let isLidarEnabled = config.sensor.isLidarEnabled;
let isDoEnabled = config.sensor.isDoEnabled;

if (isDoEnabled.isMultiData) {
    doMultiData = readCsv(doMultiDataJson);  
    doMultiJson = JSON.parse(doMultiData);
}

let msgData = {};
let s1 = 0,
    s2 = 0,
    s3 = 0,
    s4 = 0,
    s5 = 0,
    s6 = 0,
    s7 = 0,
    s8 = 0,
    s9 = 0,
    s10 = 0,
    s11 = 0,
    s12 = 0,
    s13 = 0,
    s14 = 0,
    s15 = 0,
    s16 = 0,
    s17 = 0,
    s18 = 0,
    s19 = 0,
    s20 = 0,
    s21 = 0,
    s22 = 0,
    s23 = 0,
    s24 = 0,
    s25 = 0,
    s26 = 0,
    s27 = 0,
    s28 = 0,
    s29 = 0,
    s30 = 0;

const cert_folder = config.certFolder
const client = MQTT.connect(config.mqttServer, {
    port: config.port,
    ca: fs.readFileSync(cert_folder + "ca.crt"),
    cert: fs.readFileSync(cert_folder + "client.crt"),
    key: fs.readFileSync(cert_folder + "client.key"),
    rejectUnauthorized: false
});

const doStuff = async () => {
    console.log("Starting");
    try {
        await client.subscribe(config.mqttTopic);
        // Cal video and data consis
        let startSimDurationTime;
        let endSimDurationTime;
        if (config.simWithVideo) {
            startSimDurationTime = Date.now();
            endSimDurationTime = Number(startSimDurationTime) + (parseInt(videoConfig.videoDuration) * 1000);
            // Write start time to file
            videoConfig.simStartTime = startSimDurationTime;
            fs.writeFileSync("video.json", JSON.stringify(videoConfig), function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("The video config was saved!");
            });

            console.log('Exe video bind camera process ...');
            simBindVideo.videoBindCamera();
        }

        const performanceStartTime = Date.now();
        const performanceEndTime = Number(performanceStartTime) + (parseInt(config.simTest.stopAfterSec) * 1000); // 8hr

        let intervalId = setInterval(async () => {
            console.log("=======================================");
            let timestamp = Date.now();
            if (config.simWithVideo) {
                if (timestamp > endSimDurationTime) {
                    clearInterval(intervalId);
                }
            }

            if (config.simTest.stopType == "time") {
                // FOR 壓測的時間設定
                if (timestamp > performanceEndTime) {
                    console.log("Time up >>>>>>>>> ", performanceEndTime);
                    if (config.simTest.processType == "pm2") {
                        let processName = 'pm2 stop ' +　config.simTest.processName;
                        console.log(processName);
                        exec(processName, function (err, stdout, stderr) {
                            res.send(header + stdout + footer);
                        });
                    }
                    clearInterval(intervalId);
                }       
            } else if (config.simTest.stopType == "count") {
                if (ncount >= config.simTest.stopAfterCount) {
                    if (config.simTest.processType == "pm2") {
                        let processName = 'pm2 stop ' +　config.simTest.processName;
                        console.log(processName);
                        exec(processName, function (err, stdout, stderr) {
                            res.send(header + stdout + footer);
                        });
                    }
                    clearInterval(intervalId);
                }
            }

            msgData = {
                "camera": [],
                "ecu": [],
                "gnss": [],
                "imu": [],
                "bms": [],
                "radar": [],
                "lidar": [],
                "DO": [],
                "is_test": config.isTest,
                "vid": config.vid,
                "time": timestamp
            };

            if (isCameraEnabled) {
                if (cameraJson[s1] == undefined) {
                    s1 = 0;
                }
                cameraJson[s1]['timestamp'] = timestamp;
                cameraJson[s1]['source_time'] = timestamp;
                cameraJson[s1]['vehicle_id'] = config.vid;
                msgData['camera'].push(cameraJson[s1]);
                s1++;
            }

            if (isEcuEnabled) {
                // accelerator pos
                if (acceleratorPosJson[s2] == undefined) {
                    s2 = 0;
                }
                acceleratorPosJson[s2]['timestamp'] = timestamp;
                acceleratorPosJson[s2]['source_time'] = timestamp;
                acceleratorPosJson[s2]['vehicle_id'] = config.vid;
                msgData['ecu'].push(acceleratorPosJson[s2]);
                s2++;

                // brake pos
                if (brakePosJson[s3] == undefined) {
                    s3 = 0;
                }
                brakePosJson[s3]['timestamp'] = timestamp;
                brakePosJson[s3]['source_time'] = timestamp;
                brakePosJson[s3]['vehicle_id'] = config.vid;
                msgData['ecu'].push(brakePosJson[s3]);
                s3++;

                // steering wheel angle
                if (steeringWheelAngleJson[s4] == undefined) {
                    s4 = 0;
                }
                steeringWheelAngleJson[s4]['timestamp'] = timestamp;
                steeringWheelAngleJson[s4]['source_time'] = timestamp;
                steeringWheelAngleJson[s4]['vehicle_id'] = config.vid;
                msgData['ecu'].push(steeringWheelAngleJson[s4]);
                s4++;

                // turn signal pos
                if (turnSignalPosJson[s5] == undefined) {
                    s5 = 0;
                }
                turnSignalPosJson[s5]['timestamp'] = timestamp;
                turnSignalPosJson[s5]['source_time'] = timestamp;
                turnSignalPosJson[s5]['vehicle_id'] = config.vid;
                msgData['ecu'].push(turnSignalPosJson[s5]);
                s5++;

                // brake light pos
                if (brakeLightPosJson[s6] == undefined) {
                    s6 = 0;
                }
                brakeLightPosJson[s6]['timestamp'] = timestamp;
                brakeLightPosJson[s6]['source_time'] = timestamp;
                brakeLightPosJson[s6]['vehicle_id'] = config.vid;
                msgData['ecu'].push(brakeLightPosJson[s6]);
                s6++;

                // engine and combine others
                if (engineLoadJson[s7] == undefined) {
                    s7 = 0;
                }
                if (coolantTempJson[s8] == undefined) {
                    s8 = 0;
                }
                if (controlModuleVoltageJson[s9] == undefined) {
                    s9 = 0;
                }
                if (intakeTempJson[s10] == undefined) {
                    s10 = 0;
                }
                if (vinJson[s11] == undefined) {
                    s11 = 0;
                }
                if (engineLightJson[s12] == undefined) {
                    s12 = 0;
                }
                if (runTimeJson[s13] == undefined) {
                    s13 = 0;
                }
                engineLoadJson[s7]['coolant_temp'] = coolantTempJson[s8]['coolant_temp']; // coolant_temp
                engineLoadJson[s7]['control_module_voltage'] = controlModuleVoltageJson[s9]['control_module_voltage']; // control_module_voltage
                engineLoadJson[s7]['intake_temp'] = intakeTempJson[s10]['intake_temp']; // intake_temp
                engineLoadJson[s7]['vin'] = vinJson[s11]['vin']; // vin
                engineLoadJson[s7]['engine_light'] = engineLightJson[s12]['engine_light']; // engine_light
                engineLoadJson[s7]['run_time'] = runTimeJson[s13]['run_time']; // run_time
                engineLoadJson[s7]['timestamp'] = timestamp;
                engineLoadJson[s7]['source_time'] = timestamp;
                engineLoadJson[s7]['vehicle_id'] = config.vid;
                msgData['ecu'].push(engineLoadJson[s7]);
                s7++;
                s8++;
                s9++;
                s10++;
                s11++;
                s12++;
                s13++;

                // main_light_pos
                if (mainLightPosJson[s14] == undefined) {
                    s14 = 0;
                }
                mainLightPosJson[s14]['timestamp'] = timestamp;
                mainLightPosJson[s14]['source_time'] = timestamp;
                mainLightPosJson[s14]['vehicle_id'] = config.vid;
                msgData['ecu'].push(mainLightPosJson[s14]);
                s14++;

                // rpm
                if (rpmJson[s15] == undefined) {
                    s15 = 0;
                }
                rpmJson[s15]['timestamp'] = timestamp;
                rpmJson[s15]['source_time'] = timestamp;
                rpmJson[s15]['vehicle_id'] = config.vid;
                msgData['ecu'].push(rpmJson[s15]);
                s15++;

                // dtc
                if (dtcJson[s16] == undefined) {
                    s16 = 0;
                }
                let dtc = [];
                dtc.push(dtcJson[s16]['dtc']);
                dtcJson[s16]['timestamp'] = timestamp;
                dtcJson[s16]['source_time'] = timestamp;
                //dtcJson[s16]['dtc'] = dtc;
                dtcJson[s16]['dtc'] = []; // TODO: for python exception
                dtcJson[s16]['vehicle_id'] = config.vid;
                //msgData['ecu'].push(dtcJson[s16]);
                s16++;

                // fuel level
                if (fuelLevelJson[s17] == undefined) {
                    s17 = 0;
                }
                fuelLevelJson[s17]['timestamp'] = timestamp;
                fuelLevelJson[s17]['source_time'] = timestamp;
                fuelLevelJson[s17]['vehicle_id'] = config.vid;
                msgData['ecu'].push(fuelLevelJson[s17]);
                s17++;

                // gear state
                if (gearStateJson[s18] == undefined) {
                    s18 = 0;
                }
                gearStateJson[s18]['timestamp'] = timestamp;
                gearStateJson[s18]['source_time'] = timestamp;
                gearStateJson[s18]['vehicle_id'] = config.vid;
                msgData['ecu'].push(gearStateJson[s18]);
                s18++;

                // speed
                if (speedJson[s26] == undefined) {
                    s26 = 0;
                }
                speedJson[s26]['timestamp'] = timestamp;
                speedJson[s26]['source_time'] = timestamp;
                speedJson[s26]['vehicle_id'] = config.vid;
                msgData['ecu'].push(speedJson[s26]);
                s26++;

                // mileage
                if (mileageJson[s28] == undefined) {
                    s28 = 0;
                }
                mileageJson[s28]['timestamp'] = timestamp;
                mileageJson[s28]['source_time'] = timestamp;
                mileageJson[s28]['vehicle_id'] = config.vid;
                msgData['ecu'].push(mileageJson[s28]);
                s28++;

                // driving mode
                if (drivingModeJson[s29] == undefined) {
                    s29 = 0;
                }
                drivingModeJson[s29]['timestamp'] = timestamp;
                drivingModeJson[s29]['source_time'] = timestamp;
                drivingModeJson[s29]['vehicle_id'] = config.vid;
                //msgData['ecu'].push(drivingModeJson[s29]);
                s29++;

            }

            // gnss
            if (isGnssEnabled) {
                if (gnssJson[s19] == undefined) {
                    s19 = 0;
                }
                let coord = [];
                coord.push(gnssJson[s19]['latitude']);
                coord.push(gnssJson[s19]['longitude']);
                coord.push(gnssJson[s19]['altitude']);
                gnssJson[s19]['coord'] = coord;
                gnssJson[s19]['timestamp'] = timestamp;
                gnssJson[s19]['source_time'] = timestamp;
                gnssJson[s19]['vehicle_id'] = config.vid;
                msgData['gnss'].push(gnssJson[s19]);
                s19++;
            }

            // imu
            if (isImuEnabled) {
                if (imuJson[s20] == undefined) {
                    s20 = 0;
                }
                imuJson[s20]['timestamp'] = timestamp;
                imuJson[s20]['source_time'] = timestamp;
                imuJson[s20]['vehicle_id'] = config.vid;
                msgData['imu'].push(imuJson[s20]);
                s20++;
            }

            // bms
            if (isBmsEnabled) {
                if (bmsJson[s21] == undefined) {
                    s21 = 0;
                }
                bmsJson[s21]['timestamp'] = timestamp;
                bmsJson[s21]['source_time'] = timestamp;
                bmsJson[s21]['vehicle_id'] = config.vid;
                msgData['bms'].push(bmsJson[s21]);
                s21++;
            }

            // lidar
            if (isLidarEnabled) {
                if (lidarJson[s22] == undefined) {
                    s22 = 0;
                }
                lidarJson[s22]['timestamp'] = timestamp;
                lidarJson[s22]['source_time'] = timestamp;
                lidarJson[s22]['vehicle_id'] = config.vid;
                msgData['lidar'].push(lidarJson[s22]);
                s22++;
            }

            // radar
            if (isRadarEnabled) {
                if (radarStatusJson[s23] == undefined) {
                    s23 = 0;
                }
                if (radarSrrJson[s24] == undefined) {
                    s24 = 0;
                }
                if (radarEsrJson[s25] == undefined) {
                    s25 = 0;
                }
                let srr = [];
                let esr = [];
                radarSrrJson[s24]['timestamp'] = timestamp;
                radarSrrJson[s24]['source_time'] = timestamp;
                radarSrrJson[s24]['vehicle_id'] = config.vid;
                radarEsrJson[s25]['timestamp'] = timestamp;
                radarEsrJson[s25]['source_time'] = timestamp;
                radarEsrJson[s25]['vehicle_id'] = config.vid;
                srr.push(radarSrrJson[s24]);
                esr.push(radarEsrJson[s25]);
                radarStatusJson[s23]['SRR'] = srr;
                radarStatusJson[s23]['ESR'] = esr;
                radarStatusJson[s23]['timestamp'] = timestamp;
                radarStatusJson[s23]['source_time'] = timestamp;
                radarStatusJson[s23]['vehicle_id'] = config.vid;
                msgData['radar'].push(radarStatusJson[s23]);
                s23++;
                s24++;
                s25++;
            }

            // DO
            if (isDoEnabled.isEnabled) {
                // Data 1 to Many
                if (isDoEnabled.isMultiData) {
                    if (doMultiJson[s30] == undefined) {
                        s30 = 0;
                    }
                    let doMultiDataObj = Object.assign({}, doMultiJson[s30]);
                    let doMultiDataArr = doMultiDataObj['DO'];
                    for (let i = 0; i < doMultiDataArr.length; i++) {
                        const element = doMultiDataArr[i];
                        doMultiDataArr[i]['timestamp'] = timestamp;
                        doMultiDataArr[i]['source_time'] = timestamp;
                        doMultiDataArr[i]['vehicle_id'] = config.vid;
                    }
                    msgData['DO'] = doMultiDataObj['DO'];
                    s30++;
                } else {
                    // Data 1 to 1
                    if (doJson[s27] == undefined) {
                        s27 = 0;
                    }
                    let objs = [];
                    let obj = {};
                    let coord = [];
                    let orien = [];
                    let dim = [];
                    let roi = {};
                    let doDataObj = Object.assign({}, doJson[s27]);
                    coord.push(doDataObj['latitude']);
                    coord.push(doDataObj['longitude']);
                    orien.push(doDataObj['ori_x']);
                    orien.push(doDataObj['ori_y']);
                    orien.push(doDataObj['ori_z']);
                    orien.push(doDataObj['ori_w']);
                    roi['wid'] = doDataObj['wid'];
                    roi['hgt'] = doDataObj['hgt'];
                    roi['step'] = doDataObj['step'];
                    roi['bigend'] = doDataObj['bigend'];
                    roi['fourcc'] = doDataObj['fourcc'];
                    roi['data'] = doDataObj['data'];
                    dim.push(doDataObj['dim_x']);
                    dim.push(doDataObj['dim_y']);
                    dim.push(doDataObj['dim_z']);
                    obj['tid'] = doDataObj['tid'];
                    obj['coord'] = coord;
                    obj['orien'] = orien;
                    obj['dim'] = dim;
                    obj['roi'] = roi;
                    objs.push(obj);
                    doDataObj['obj'] = objs;
                    doDataObj['timestamp'] = timestamp;
                    doDataObj['source_time'] = timestamp;
                    doDataObj['vehicle_id'] = config.vid;
                    delete doDataObj['tid'];
                    delete doDataObj['latitude'];
                    delete doDataObj['longitude'];
                    delete doDataObj['altitude'];
                    delete doDataObj['ori_x'];
                    delete doDataObj['ori_y'];
                    delete doDataObj['ori_z'];
                    delete doDataObj['ori_w'];
                    delete doDataObj['dim_x'];
                    delete doDataObj['dim_y'];
                    delete doDataObj['dim_z'];
                    delete doDataObj['wid'];
                    delete doDataObj['hgt'];
                    delete doDataObj['step'];
                    delete doDataObj['bigend'];
                    delete doDataObj['fourcc'];
                    delete doDataObj['data'];
                    delete doDataObj['vehicle_id'];
                    msgData['DO'].push(doDataObj);
                    s27++;
                }
                
            }
            await client.publish(config.mqttTopic, JSON.stringify(msgData));
        }, simIntervaMillisec);
    } catch (e) {
        console.log(e.stack);
        process.exit();
    }
};

client.on("connect", doStuff);
client.on("message", (topic, msg, request) => {
    console.log("topic: ", topic + " --- " + Date.now());
    console.log("msg: ", msg.toString());
    console.log("count:", ncount);
    ncount++;
});

function readCsv(pathname) { 
    var bin = fs.readFileSync(pathname); 
    if (bin[0] === 0xEF && bin[1] === 0xBB && bin[2] === 0xBF) { 
        bin = bin.slice(3); 
    } 
    return bin.toString('utf-8'); 
}
