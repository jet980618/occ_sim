const ffmpeg = require('fluent-ffmpeg');
const config = require('./config/config.json');

const videoPath = config.video.videoPath;
const encodeOutVideoPath = config.video.encodeOutVideoPath;

const encodeVideo = async function (videoPath, outVideoPath) {
    return new Promise((resolve, reject) => {
        ffmpeg(videoPath).videoCodec('libx264')
            .on('progress', (progress) => {
                console.log(`[ffmpeg] ${JSON.stringify(progress)}`);
            })
            .on('error', (err) => {
                console.log(`[ffmpeg] error: ${err.message}`);
                reject(err);
            })
            .on('end', () => {
                console.log('[ffmpeg] finished');
                resolve();
            })
            .save(outVideoPath);
    });
}

encodeVideo(videoPath, encodeOutVideoPath);