const http = require('http');
const request = require('request');
const fs = require('fs');
const config = require('./config/config.json');
const videoConfig = require('./video.json');

const videoPath = config.video.videoPath;
const encodeOutVideoPath = config.video.encodeOutVideoPath;
const cameraName = config.video.cameraName;
const uploadId = videoConfig.uploadId;
const ttl = videoConfig.ttl; 
const chunkSize = videoConfig.chunkSize;
const nxHostApiUrl = 'http://' + config.video.nxServerAuth + '@' + config.video.nxServerHost + ':' + config.video.nxServerPort;
const getCamerasExUrl = '/ec2/getCamerasEx';
const getUsersUrl = '/ec2/getUsers';
const nxDownloadApiUrl = '/api/downloads';
const nxCameraApiUrl = '/api/wearableCamera';
let videoUploadRetry = videoConfig.videoUploadRetry;
let url = '';
let options = {
    hostname: config.video.nxServerHost,
    port: config.video.nxServerPort,
    auth: config.video.nxServerAuth
};

if (videoUploadRetry == '' || videoUploadRetry === undefined) {
    videoUploadRetry = 3;
}

const videoBindCamera = async function () {
    let userId = '';
    let cameraId = '';
    let token = '';

    // 1-1 Get NX admin id
    console.log('1-1 Get NX admin id ...');
    options.path = getUsersUrl;
    let userData = await httpRequest(options);
    for (let i = 0; i < userData.length; i++) {
        if (userData[i].name === 'admin') {
            userId = userData[i].id.replace('{', '').replace('}', '');
        }
    }
    console.log('User id: ' + userId);
    if (userId == '') {
        console.log('NX server admin not found!');
        return;
    }

    // 1-2 Get camera id by name
    console.log('1-2 Get camera id ...');
    options.path = getCamerasExUrl;
    let cameraData = await httpRequest(options);
    for (let i = 0; i < cameraData.length; i++) {
        if (cameraData[i].name === cameraName) {
            cameraId = cameraData[i].id.replace('{', '').replace('}', '');
        }
    }
    console.log('Camera id: ' + cameraId);
    if (cameraId == '') {
        console.log('Camera not found!');
        return;
    }

    // 3-1 Lock the camera
    console.log('3-1 Lock the camera ...');
    url = nxHostApiUrl + nxCameraApiUrl + '/lock?' + 'cameraId=' + cameraId + '&userId=' + userId + '&ttl=' + ttl;
    let lockCameraOptions = {
        method: 'POST',
        url: url
    };
    let lockCameraData = await httpRequest2(lockCameraOptions);
    if (lockCameraData.statusCode != 200) {
        console.log('Lock camera failed! ');
        return;
    }
    let lockCameraResJson = JSON.parse(lockCameraData.body);
    console.log('Camera lock status : ' + lockCameraResJson.reply.locked);
    if (lockCameraResJson.reply.locked !== true) {
        console.log('Camera locked fail!');
        return;
    }
    token = lockCameraResJson.reply.token;
    console.log('Get token: ' + token);
    if (token === '' || token === undefined) {
        console.log('Token not found!');
        return;
    }
    token = token.replace('{', '').replace('}', '');

    // 3-2 Import the file to camera
    console.log('3-2 Import the file to camera ...');
    if (videoConfig.simStartTime == '') {
        console.log('The sim start time is empty! ');
        return;
    }
    url = nxHostApiUrl + nxCameraApiUrl + '/consume?' + 'cameraId=' + cameraId + '&token=' + token + '&startTime=' + videoConfig.simStartTime + '&uploadId=' + uploadId;
    console.log('Url >>> ' + url);
    let importFileOptions = {
        method: 'POST',
        url: url
    };
    let importFileData = await httpRequest2(importFileOptions);
    //console.log(JSON.stringify(importFileData));
    if (importFileData.statusCode != 200) {
        let importFileResJson = JSON.parse(importFileData.body);
        console.log('Import the file to camera status code: ' + importFileData.statusCode);
        console.log('Import the file to camera error: ' + importFileResJson.errorString);
        return;
    }

    // 3-3 Monitor progress
    console.log('3-3 Monitor progress ...');
    let mpSuccess = false;
    let retryLimit = 100;
    while (!mpSuccess) {
        url = nxHostApiUrl + nxCameraApiUrl + '/extend?' + 'cameraId=' + cameraId + '&token=' + token + '&userId=' + userId + '&ttl=' + ttl;
        console.log('Url >>> ' + url);
        let monitorProgressOptions = {
            method: 'POST',
            url: url
        };
        let monitorProgressData = await httpRequest2(monitorProgressOptions);
        let mpResJson = JSON.parse(monitorProgressData.body);
        if (monitorProgressData.statusCode != 200) {
            console.log('Monitor progress error: ' + mpResJson.errorString);
            return;
        }
        console.log('Monitor progress : ' + mpResJson.reply.progress + '%' + '/ suceess status: ' + mpResJson.reply.success);
        if (mpResJson.reply.success == true) {
            mpSuccess = true;
        }
        if (retryLimit > 100) {
            mpSuccess = true;
        }
        retryLimit++;
    }

    // 3-4 Release the camera
    console.log('3-4 Release the camera ...');
    url = nxHostApiUrl + nxCameraApiUrl + '/release?' + 'cameraId=' + cameraId + '&token=' + token;
    console.log('Url >>> ' + url);
    let releaseCameraOptions = {
        method: 'POST',
        url: url
    };
    let releaseCameraData = await httpRequest2(releaseCameraOptions);
    let releaseCameraResJson = JSON.parse(releaseCameraData.body);
    if (releaseCameraData.statusCode != 200) {
        console.log('Release camera error: ' + releaseCameraResJson.errorString);
        return;
    }
    console.log('Release camera success: ' + releaseCameraResJson.reply.success + '/ locked: ' + releaseCameraResJson.reply.locked);

    videoConfig.cameraId = cameraId;
    fs.writeFileSync("video.json", JSON.stringify(videoConfig), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The video config was saved!");
    });
}

const httpRequest = async function (option) {
    return new Promise((resolve, reject) => {
        var req = http.request(option, (res) => {
            //console.log(`STATUS: ${res.statusCode}`);
            //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
            res.setEncoding('utf8');
            let resData = [];
            res.on('data', (chunk) => {
                resData += chunk;
            });
            res.on('end', () => {
                let jsonObj = '';
                try {
                    jsonObj = JSON.parse(resData);
                } catch (e) {
                    reject(e);
                    return;
                }
                resolve(jsonObj);
            })
        });
        req.on('error', (e) => {
            console.log(`problem with request: ${e.message}`);
            reject(e);
        });
        req.end();
    });
}

const httpRequest2 = async function (option) {
    return new Promise((resolve, reject) => {
        request(option, function (error, response, body) {
        }).on('response', function (response) {
            resolve(response);
        }).on('error', function (error) {
            console.log(`problem with request: ${error.message}`);
            reject(error);
        });
    });
}

// videoBindCamera();

module.exports = {
    videoBindCamera
};