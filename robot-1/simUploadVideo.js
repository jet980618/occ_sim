const splitFile = require('split-file');
const md5File = require('md5-file');
const ffmpeg = require('fluent-ffmpeg');
const request = require('request');
const fs = require('fs');
const uuid = require('uuid');
const config = require('./config/config.json');
const videoConfig = require('./video.json');

const videoPath = config.video.videoPath;
const encodeOutVideoPath = config.video.encodeOutVideoPath;
const cameraName = config.video.cameraName;
const uploadId = videoConfig.uploadId;
const ttl = videoConfig.ttl; 
const chunkSize = videoConfig.chunkSize;
const nxHostApiUrl = 'http://' + config.video.nxServerAuth + '@' + config.video.nxServerHost + ':' + config.video.nxServerPort;
const getCamerasExUrl = '/ec2/getCamerasEx';
const getUsersUrl = '/ec2/getUsers';
const nxDownloadApiUrl = '/api/downloads';
let videoUploadRetry = videoConfig.videoUploadRetry;
let url = '';
let options = {
    hostname: config.video.nxServerHost,
    port: config.video.nxServerPort,
    auth: config.video.nxServerAuth
};

if (videoUploadRetry == '' || videoUploadRetry === undefined) {
    videoUploadRetry = 3;
}

const videoSplitAndUpload = async function () {
    // 0-1 Gen md5sum
    console.log('0-1 Gen md5 ...');
    const md5sum = md5File.sync(videoPath)
    console.log('The MD5 sum of ' + videoPath + ' is: ' + md5sum);

    // 0-2 Split video
    let splitVideoArr = [];
    console.log('0-2 Split file ...');
    await splitFile.splitFileBySize(videoPath, chunkSize)
        .then((names) => {
            console.log(names);
            splitVideoArr = names;
        })
        .catch((err) => {
            console.log('Error: ', err);
        });

    // 0-3 Get video info
    console.log('0-3 Get video info ...');
    let metadata = await getVideoMeta(videoPath);
    let videoDuration = metadata.format.duration;
    let videoSize = metadata.format.size;
    console.log('Video duration: ' + videoDuration);
    console.log('Video size: ' + videoSize);

    // 2-1 Create a new upload job
    console.log('2-1 Create a new upload job ...');
    // let createUploadJobUrl = '/api/downloads/test032?size=3243309&chunkSize=10000000&md5=92d1fe4da19688a6fdf95a505fce686f&ttl=86400000&upload=true';
    let vSize = videoSize;
    // Gen upload id by uuid
    let genUuid = uuid.v1();
    url = nxHostApiUrl + nxDownloadApiUrl + '/' + genUuid + '?size=' + vSize + '&chunkSize=' + chunkSize + '&md5=' + md5sum + '&ttl=' + ttl + '&upload=true';
    console.log('Gen uuid: ' + genUuid);
    console.log('Url >>> ' + url);
    let createUploadOptions = {
        method: 'POST',
        url: url
    };
    let uploadJobData = await httpRequest2(createUploadOptions);
    if (uploadJobData.statusCode != 200) {
        let uploadJobResJson = JSON.parse(uploadJobData.body);
        console.log('Create a upload job failed! ' + uploadJobResJson.errorString);
        return;
    }

    // 2-2 Upload chunks
    console.log('2-2 Upload chunks ...');
    // admin:Wistron168@140.110.141.115:7001/api/downloads/test032/chunks/0
    for (let i = 0; i < splitVideoArr.length; i++) {
        console.log('Video uploading: >>>>>> ' + i + ' / ' + splitVideoArr[i]);
        url = nxHostApiUrl + nxDownloadApiUrl + '/' + genUuid + '/chunks/' + i;
        let payload = await fs.readFileSync(splitVideoArr[i]);
        let uploadChunkOptions = {
            method: 'PUT',
            url: url,
            headers: { 'Content-Type': 'application/octet-stream' },
            body: payload,
        };

        let uploadChunkData;
        let uploadRetry = 1;
        while (uploadRetry <= videoUploadRetry) {
            try {
                uploadChunkData = await httpRequest2(uploadChunkOptions);
                break;
            } catch (error) {
                console.log(error);
                console.log("Excute upload retry: " + uploadRetry);
                uploadRetry++;
            }
        }

        if (uploadChunkData.statusCode != 200) {
            let uploadChunkResJson = JSON.parse(uploadChunkData.body);
            console.log('Upload video chunk failed! ' + splitVideoArr[i]);
            console.log('Upload error: ' + uploadChunkResJson.errorString);
            return;
        }
    }

    // 2-3 Validate the file upload
    console.log('2-3 Validate the file upload status ...');
    url = nxHostApiUrl + nxDownloadApiUrl + '/' + genUuid + '/status';
    let validUploadOptions = {
        method: 'GET',
        url: url
    };
    let validUploadData = await httpRequest2(validUploadOptions);
    let validUploadResJson = JSON.parse(validUploadData.body);
    if (validUploadData.statusCode != 200) {
        console.log('Validate upload failed: ' + validUploadResJson.errorString);
        return;
    }
    console.log('Upload video status: ' + validUploadResJson.reply.status);
    if (validUploadResJson.reply.status !== 'downloaded') {
        console.log('Upload video not ready!');
        return;
    }

    videoConfig.videoDuration = videoDuration;
    videoConfig.videoSize = videoSize;
    videoConfig.uploadId = genUuid;
    fs.writeFileSync("video.json", JSON.stringify(videoConfig), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The video config was saved!");
    });
}

const httpRequest2 = async function (option) {
    return new Promise((resolve, reject) => {
        request(option, function (error, response, body) {
        }).on('response', function (response) {
            resolve(response);
        }).on('error', function (error) {
            console.log(`problem with request: ${error.message}`);
            reject(error);
        });
    });
}

const getVideoMeta = async function (videoPath) {
    return new Promise((resolve, reject) => {
        ffmpeg.ffprobe(videoPath, function (err, metadata) {
            resolve(metadata);
        });
    });
}

videoSplitAndUpload();

module.exports = {
    videoSplitAndUpload
};